#pragma once
#ifndef NODE_H
#define NODE_H

class Node
{
public:
	int number;
	Node* parent1;
	Node* parent2;
	void setParent(Node* _parent);

	Node();
	// overload Node-Constructor
	Node(int _number);
	Node (int _number, Node* _parent1, Node* _parent2);
	~Node();
};

#endif