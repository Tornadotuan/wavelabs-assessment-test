#pragma once
#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include "Node.h"


using namespace std;

//hello

class Hashtable {
	

public:

	unordered_map<int, Node*> htmap;

	void putNodetoKey(int key, Node *value) {
		htmap[key] = value;
	}

	Node *getNodeFromKey(int key) {
		return htmap[key];
	}

	int returnSize() {
		return htmap.size();
	}
};


void pause()
{
	cout << "Please press Enter! \n" << endl;
	getchar();
}



int main() {

	int pairs[][2] = { {10, 3}, {2, 3}, {3, 6}, {5, 6}, {5, 17},{4, 5}, {4, 8}, {8, 9}, {3, 1}, {4,6}, {10, 10}, {99,33}, {5,5}, {7,3} };
	int arraySize = sizeof pairs / sizeof pairs[0];

	printf("This is a programm for Parent-Children Relationship");

	

	Hashtable myHash;

	cout << "Number of Pairs: " << arraySize << "\n" << endl;

	for (int i = 0; i < arraySize; i++) {

		int currentChild	= pairs[i][1];
		int currentParent	= pairs[i][0];

		cout << "{" << currentParent << "," << currentChild << "}" << endl;

		if (currentParent != currentChild) {

			if (myHash.getNodeFromKey(currentChild) == NULL) {

				Node* newNode = new Node(currentChild);

				myHash.putNodetoKey(currentChild, newNode);

				// cout << "Create Nodenumber: " << myHash.getNodeFromKey(currentChild)->number << endl;
			}

			if (myHash.getNodeFromKey(currentParent) == NULL) {

				Node* newNode = new Node(currentParent);

				myHash.putNodetoKey(currentParent, newNode);

				//cout << "Create Nodenumber: " << myHash.getNodeFromKey(currentParent)->number << endl;
			}
			myHash.getNodeFromKey(currentChild)->setParent(myHash.getNodeFromKey(currentParent));
		}
		else {
			cout << "Fehler bei: " << currentParent << " -- " << currentChild  << "! \n" << endl;
		}
	}

	cout << "\n \n";

	for (const auto i: myHash.htmap) {


		if (i.second->parent1 == NULL) {

			cout << "ZeroParent "<< i.first << endl;
		}

	}
	
	cout << "\n \n";

	for (const auto i : myHash.htmap) {

		if (i.second->parent1 != NULL && i.second->parent2 == NULL) {
			cout << "OneParent " << i.first << endl;
		}
	}





	/*
		if (!nodeNameToNodeMap.ContainsKey(child))
			nodeNameToNodeMap[child] = new TreeNode(child);
		if (!nodeNameToNodeMap.ContainsKey(parent))
			nodeNameToNodeMap[parent] = new TreeNode(parent);

		nodeNameToNodeMap[parent].children.Add(nodeNameToNodeMap[child]);//add a to b's children
		nodeNameToNodeMap[child].parent = nodeNameToNodeMap[parent];//mark a as b's child

	

	*/


	pause();

	return 0;
}



