#include "Node.h"
#include <iostream>

using namespace std;

//test

void Node::setParent(Node* _parent)
{
	if (this->parent1 == NULL) {

		this->parent1 = _parent;

	}
	else if (this->parent2 == NULL) {
		this->parent2 = _parent;
	}
	else {
		cout << "\n Node " << this->number << " has more than 2 parents! \n";
	}
}

Node::Node()
{
		this->number	= 0;
		this->parent1	= NULL;
		this->parent2	= NULL;
}

Node::Node(int _number)
{
	this->number = _number;
	this->parent1 = NULL;
	this->parent2 = NULL;
}

Node::Node(int _number, Node* _parent1, Node* _parent2)

{
	this->number = _number;
	this->parent1 = _parent1;
	this->parent2 = _parent2;
}

Node::~Node()
{
}

